# -*- coding: utf-8 -*-
"""
Created on Sat May 12 10:06:26 2018

@author: udayk
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#importing dataset
import os
os.chdir('/home/uday/intern/Fwd_ Previous msgs Bitxoxo')
dataset = pd.read_csv('bitxoxoNew.csv')
#dataset = pd.read_csv('C:/Users/udayk/Desktop/intern/Fwd_ Previous msgs Bitxoxo bitxoxoNew.csv')

#splitting messages into a list
import re
dataset['split_messages'] = dataset['Message'].apply(lambda x:((re.sub('[^a-zA-Z]',' ',x)).lower()).split())   

#dropping any messages whose length doesn't meet the criteria
dataset = dataset.drop(dataset[(dataset['split_messages'].apply(lambda x: len(x)) == 0)|(dataset['split_messages'].apply(lambda x: len(x)) > 50)].index)

#dropping bots 
dataset = dataset.drop(dataset[(dataset['UserName'].apply(lambda x: bool(re.search('bot',x.lower()) or re.search('admin',x.lower()) or re.search('team',x.lower()))) == True)].index)
dataset = dataset.drop(dataset[(dataset['First_Name'].apply(lambda x: bool(re.search('bot',x.lower()) or re.search('admin',x.lower()) or re.search('team',x.lower()))) == True)].index)

#creating the matrix
features = ['ico','futures','options','swap','scam','liquidity','dividend','profit','negative','beta','operation','live','launch','spreads','derivatives','leverage','stoploss','trailing','limit','matrix','ladder','license','sec','regulated']
col_names = ['UserID','message_count']+features+['total_count']
matrix = pd.DataFrame(columns = col_names)

grouped = dataset.groupby('UserID')
for name,group in grouped:
    message_count = len(group)
    list_gen = []
    for feature in features:
        list_gen.append(sum(group['split_messages'].apply(lambda x: feature in x)))
    total_count = sum(list_gen)
    list_to = pd.DataFrame([[name,message_count]+list_gen+[total_count]],columns = col_names)
    
    
    matrix = matrix.append(list_to)
    
del(feature,features,group,list_gen,list_to,message_count,name,total_count)
#sorting them as per total message count
matrix = matrix.sort_values(by = 'total_count' , ascending = False)
writer = pd.ExcelWriter('BitXOXO_user.xlsx', engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
matrix.to_excel(writer, sheet_name='Sheet1')

# Close the Pandas Excel writer and output the Excel file.
writer.save()

import nltk

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
questions = ['what','why','how','where','when','who','whom','?']
prep = ['do','can','does','are','is','am']
cols = ['UserID','noqs','questions']
questions_matrix = pd.DataFrame(columns = cols)
for name,group in grouped:
    sentences = group['Message'].apply(lambda x: sent_tokenize(x.lower()))
    sentences = sentences.apply(pd.Series).stack().reset_index(drop=True)
    words = sentences.apply(lambda x: word_tokenize(x))
    df = pd.concat([sentences, words],axis=1)
    df['questions'] = df[1].apply(lambda x:bool(len(list(set(questions).intersection(x)))!=0) or x[0] in prep)
    df = df.drop(df[df.questions == False].index)
    df = df.drop(df[df[1].apply(lambda x:len(x))<4].index)
    list_q = df[0].tolist()
    list_to = pd.DataFrame([[name,len(list_q),list_q]],columns = cols)
    questions_matrix = questions_matrix.append(list_to)
del(name,group,sentences,words,list_q,list_to,df)    
questions_matrix = questions_matrix.sort_values(by= 'noqs',ascending = False)

questions_matrix = questions_matrix[questions_matrix.noqs != 0]
writer = pd.ExcelWriter('BitXOXO_user_questions.xlsx', engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
questions_matrix.to_excel(writer, sheet_name='Sheet1')

# Close the Pandas Excel writer and output the Excel file.
writer.save()
dataset = pd.read_csv('BitxoxoMsgData2.csv')
reply = dataset[dataset.Reply_to_msgid != ' None ']

reply['Reply_to_msgid'] = reply['Reply_to_msgid'].apply(lambda x: int(x))
reply['reply'] = reply['Reply_to_msgid'].apply(lambda x: x in dataset['MesNo'])
reply = reply[reply.reply != False]
list_p = reply['Reply_to_msgid'].tolist()
list_to = []
for p in list_p:
    reply_i = dataset['Message'][(dataset['MesNo']== p).idxmax()]
    list_to.append(reply_i)
    
dfr = pd.Series(list_to)
reply['reply_to'] = dfr