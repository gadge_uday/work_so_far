# -*- coding: utf-8 -*-
"""
Created on Tue May 22 15:20:57 2018

@author: udayk
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#importing 
import os
os.chdir('/home/uday/intern/Fwd_ Previous msgs Bitxoxo')
dataset = pd.read_csv('digitexNew.csv')

dataset = dataset.drop(['MesNo','MesId','UserID','Reply_to_msgid','Message'],axis =1)

from datetime import datetime
dataset['DateTime'] = pd.to_datetime(dataset['DateTime'])
dataset = dataset.rename(columns={'Unnamed: 0':'count'})
dataset.index = dataset['DateTime']
del dataset['DateTime']
dataset = dataset.groupby(level = 0).count()
daily_count = dataset.resample('D').sum()
daily_count.plot()
dataset.index = dataset.index.values.astype('<M8[m]')
dataset['Time'] = dataset.index.time
dataset.index = dataset['Time']
del dataset['Time']
time_data = dataset.groupby(level = 0).sum()
time_data['count'] = time_data['count'].apply(lambda x:x/153)
daily_count.plot()
time_data.plot()
time_data['Time'] = time_data.index
time_data['Time'] = pd.to_datetime(dataset)
time_data.plot()