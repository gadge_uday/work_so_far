# -*- coding: utf-8 -*-
"""
Created on Sat May 12 10:06:26 2018

@author: udayk
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#importing 
import os
os.chdir('/home/uday/intern/Fwd_ Previous msgs Bitxoxo')
dataset = pd.read_csv('binanceNew.csv')


#splitting messages into a list
import re
dataset['split_messages'] = dataset['Message'].apply(lambda x:((re.sub('[^a-zA-Z]',' ',x)).lower()).split())   

#dropping any messages whose length doesn't meet the criteria
dataset = dataset.drop(dataset[(dataset['split_messages'].apply(lambda x: len(x)) == 0)|(dataset['split_messages'].apply(lambda x: len(x)) > 50)].index)
dataset_full = dataset
#dropping bots 
dataset = dataset.drop(dataset[(dataset['UserName'].apply(lambda x: bool(re.search('bot',x.lower()) or re.search('admin',x.lower()) or re.search('team',x.lower()))) == True)].index)
dataset = dataset.drop(dataset[(dataset['First_Name'].apply(lambda x: bool(re.search('bot',x.lower()) or re.search('admin',x.lower()) or re.search('team',x.lower()))) == True)].index)

#creating the matrix
features = ['ico','futures','options','swap','scam','liquidity','dividend','profit','negative','beta','operation','live','launch','spreads','derivatives','leverage','stoploss','trailing','limit','matrix','ladder','license','sec','regulated']
col_names = ['UserID','message_count']+features+['total_count']
matrix = pd.DataFrame(columns = col_names)

grouped = dataset.groupby('UserID')
for name,group in grouped:
    message_count = len(group)
    list_gen = []
    for feature in features:
        list_gen.append(sum(group['split_messages'].apply(lambda x: feature in x)))
    total_count = sum(list_gen)
    list_to = pd.DataFrame([[name,message_count]+list_gen+[total_count]],columns = col_names)
    
    
    matrix = matrix.append(list_to)
    
del(feature,features,group,list_gen,list_to,message_count,name,total_count)
#sorting them as per total message count
matrix = matrix.sort_values(by = 'total_count' , ascending = False)
writer = pd.ExcelWriter('utrumio_user.xlsx', engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
matrix.to_excel(writer, sheet_name='Sheet1')

# Close the Pandas Excel writer and output the Excel file.
writer.save()







#question_extraction.................
import nltk

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
questions = ['what','why','how','where','when','who','whom','?']
prep = ['do','can','does','are','is','am']
cols = ['MessNo','UserID','noqs','questions']
questions_matrix = pd.DataFrame(columns = cols)
list = []
for name,group in grouped:
    group['sentences'] = group['Message'].apply(lambda x: sent_tokenize(x.lower()))
    group['no'] = group['sentences'].apply(lambda x: len(x))
    group['lists'] = group.apply(lambda row: (lambda x,y: [x]*y)(row['MesNo'], row['no']), axis=1)
    df = pd.DataFrame(columns = cols)
    df['questions'] = group['sentences'].apply(pd.Series).stack().reset_index(drop=True).tolist()
    df['UserID'] = [name]*len(df)
    df['MessNo'] = sum(group['lists'], [])
    df['words'] = df['questions'].apply(lambda x: word_tokenize(x))
    df['check'] = df['words'].apply(lambda x:bool(len(set(questions).intersection(x))!=0) or x[0] in prep)
    df = df.drop(df[df.check == False].index)
    del(df['words'],df['check'])
    df['noqs'] = [len(df)]*len(df)
    questions_matrix = questions_matrix.append(df)

del(cols,df,prep,questions,name,group)   
questions_matrix = questions_matrix.sort_values(by= 'noqs',ascending = False)

questions_matrix = questions_matrix.drop(questions_matrix[(questions_matrix['questions'].apply(lambda x:bool(re.search('http',x) == True or len(x) == 1)))].index)


writer = pd.ExcelWriter('digitex_user_questions.xlsx', engine='xlsxwriter')
questions_matrix.to_excel(writer, sheet_name='Sheet1')
writer.save()

questions_matrix = pd.read_excel('binance_user_questions.xlsx')
#.......end.........






dataset = pd.read_csv('BitxoxoMsgData2.csv')
reply = dataset[dataset.Reply_to_msgid != ' None ']
reply['Reply_to_msgid'] = reply['Reply_to_msgid'].apply(lambda x: int(x))
reply['reply'] = reply['Reply_to_msgid'].apply(lambda x: x in dataset['MesId'])
reply = reply[reply.reply != False]
reply['reply_to'] = reply['Reply_to_msgid'].apply(lambda x: dataset['Message'][dataset['MesId'][dataset['MesId']==x].index])
#list_p = reply['Reply_to_msgid'].tolist()
#list_to = []
#for p in list_p:
#    reply_i = dataset['Message'][(dataset['MesId']== p).idxmax()]
#    list_to.append(reply_i)
#    
#dfr = pd.Series(list_to)
#reply['reply_to'] = dfr
#del(dfr,list_p,list_to,p,reply_i,reply['split_messages'],reply['reply'])

writer = pd.ExcelWriter('BitXOXO_user_replies.xlsx', engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
reply.to_excel(writer, sheet_name='Sheet1')

# Close the Pandas Excel writer and output the Excel file.
writer.save()



questions = questions_matrix.sort_values(by = 'MessNo')
questions.index = range(0,len(questions))

from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))
stop_words = stop_words.union({'?','hi','hello','thanks','welcome','really','okay','sure','anybody','anyone','everyone','everybody','guy','guys'})
questions['words'] = questions['questions'].apply(lambda x: word_tokenize(re.sub('[^a-zA-Z0-9]+',' ',x)))
questions['filter'] = questions['questions'].apply(lambda x: [w for w in word_tokenize(re.sub('[^a-zA-Z0-9]+',' ', x).lower()) if not w in stop_words])
questions['filter'] = questions['filter'].apply(lambda x: [wordnet_lemmatizer.lemmatize(y) for y in x])
questions = questions.drop(questions[questions['filter'].apply(lambda x: bool(len(x)==0))].index)
questions['filter_sentence'] = questions['filter'].apply(lambda x: ' '.join(x))
questions['tagged'] = questions['filter'].apply(lambda x: nltk.pos_tag(x))



def process(tag):
    chunkGram = r"""Chunk: {<NN>?}"""
    chunkParser = nltk.RegexpParser(chunkGram)
    chunked = chunkParser.parse(tag)
    for subtree in chunked.subtrees(filter=lambda t: t.label() == 'Chunk'):
                return (subtree)
            
questions['chunks'] = questions['tagged'].apply(lambda x:process(x))
questions_chunked = questions.drop(questions[questions['chunks'].apply(lambda x:x==None)].index)  
questions_chunked['chunks'] = questions['chunks'].apply(lambda x:str(x))
questions_chunked['chunks'] = questions_chunked['chunks'].apply(lambda x: x[7:len(x)-4])


group_words = questions_chunked.groupby('chunks')
words = pd.DataFrame(columns = ['chunk','frequency'])
for name,group in group_words:
    words = words.append(pd.DataFrame([[name,len(group)]],columns = ['chunk','frequency']))

words = words.sort_values(by = 'frequency',ascending = False)
import os

os.chdir('/home/uday/intern/Fwd_ Previous msgs Bitxoxo')
writer = pd.ExcelWriter('digitex_words.xlsx', engine='xlsxwriter')
words.to_excel(writer, sheet_name='Sheet1')
writer.save()

words_needed = ['deposit','bnb','xvg','withdrawl','login','sell','exchange','trading','airdrop','xrp','tron','wallet']
datafr = group_words.get_group('binance')
for word in words_needed:
    datafr = datafr.append(group_words.get_group(word))

admins_list = [409464881,344347398,314628880,447912744,450657351,422991730,213904490]

def replies(mylist = [],*arg):
    messages = []
    for item in mylist:
        index = admin_messages['MesNo'][admin_messages['MesNo']==item].index[0]
        message = admin_messages['Message'][index]
        messages.append(message)
    return messages

datafr = datafr.rename(columns={'MessNo':'MesNo'})
datafr = datafr.merge(dataset.drop('UserID',axis=1),on='MesNo')
#datafr = datafr.drop(['split_messages','Reply_to_msgid','Message'],axis = 1)
datafr = datafr[~datafr['UserID'].isin(admins_list)]
admin_messages = dataset[dataset['UserID'].isin(admins_list)]
datafr['reply_ids'] = datafr['MesNo'].apply(lambda x:set(range(x+1,x+6)))
datafr['check'] = datafr['reply_ids'].apply(lambda x:x.intersection(set(admin_messages['MesNo'])))
datafr_questions = datafr[datafr['check'].map(len)>0]
datafr_questions['check'] = datafr_questions['check'].apply(lambda x:list(x))
#datafr_questions['replies'] = datafr_questions['check'].apply(lambda x: admin_messages['MesNo'][admin_messages['MesNo']==x].index[0])
#datafr_questions['replies'] = datafr_questions['replies'].apply(lambda x:admin_messages['Message'][x])
datafr_questions['replies'] = datafr_questions['check'].apply(lambda x: replies(x))
datafr_questions = datafr_questions.drop(['noqs','filter','filter_sentence','tagged','chunks','DateTime'],axis = 1)
datafr_questions = datafr_questions.drop(['Unnamed: 0','Reply_to_msgid','questions','split_messages','reply_ids','check'],axis = 1)
datafr_questions = datafr_questions.drop(['words'],axis = 1)

writer = pd.ExcelWriter('binance_replies.xlsx', engine='xlsxwriter')
datafr_questions.to_excel(writer, sheet_name='Sheet1')
writer.save()





