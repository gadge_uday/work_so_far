# -*- coding: utf-8 -*-
"""
Created on Fri May 25 12:28:01 2018

@author: udayk
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))
stop_words = stop_words.union({'?','hi','hello','thanks','welcome','really','okay','sure','anybody','anyone','everyone','everybody','guy','guys','lol'})
import os
os.chdir('/home/uday/intern/Fwd_ Previous msgs Bitxoxo')
dataset = pd.read_csv('digitexNew.csv')

dataset = dataset.drop(['MesNo','UserID','Reply_to_msgid','Unnamed: 0'],axis =1)
from datetime import datetime
dataset['DateTime'] = pd.to_datetime(dataset['DateTime'])
dataset = dataset.rename(columns={'Unnamed: 0':'count'})
dataset.index = dataset['DateTime']
del dataset['DateTime']
dataset_jan15 = dataset['2018-01-15']
dataset_jan15['Message'] = dataset_jan15['Message'].apply(lambda x: (re.sub('[^a-zA-Z]',' ',x)).lower())

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
def display_topics(H, W, feature_names, documents, no_top_words, no_top_documents):
    for topic_idx, topic in enumerate(H):
        print ("Topic %d:" % (topic_idx))
        print (" ".join([feature_names[i]
                        for i in topic.argsort()[:-no_top_words - 1:-1]]))
        top_doc_indices = np.argsort( W[:,topic_idx] )[::-1][0:no_top_documents]
        for doc_index in top_doc_indices:
            print (documents[doc_index])

n_features = 500
n_components = 10
tfidf_vectorizer = TfidfVectorizer(max_df=0.85, min_df=5,
                                   max_features=n_features,
                                   stop_words=stop_words,strip_accents = 'ascii')
tfidf = tfidf_vectorizer.fit_transform(dataset_jan15['Message'])
nmf = NMF(n_components=n_components, random_state=1,
          alpha=.1, l1_ratio=.5).fit(tfidf)
nmf_W = nmf.transform(tfidf)
nmf_H = nmf.components_
no_top_words = 3
no_top_documents = 3
tfidf_feature_names = tfidf_vectorizer.get_feature_names()
display_topics(nmf_H, nmf_W, tfidf_feature_names, dataset_jan15['Message'], no_top_words, no_top_documents)
